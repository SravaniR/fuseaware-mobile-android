package com.fuseaware.mobile.tests;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class HomeTest {
    static AndroidDriver<AndroidElement> driver;
    SoftAssert sAssert = new SoftAssert();
    private AndroidElement element;

    @BeforeClass
    public void setup() throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("deviceName", "Android");
        cap.setCapability("platformName", "Android");
        cap.setCapability("platformVersion", "7.0");
        cap.setCapability("noReset", "false");
        cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.fuseaware.fuseaware");
        cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "LauncherActivity");
        driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        System.out.println(driver.getContext());
    }

    @Test(priority = 1)
    public void Login() throws Exception {
        driver.findElement(By.xpath("//android.widget.EditText[@resource-id='com.fuseaware.fuseaware:id/email_or_phone']")).sendKeys("ren@fuseaware.com"); // email id
        driver.findElement(By.xpath("//android.widget.EditText[@resource-id='com.fuseaware.fuseaware:id/password']")).sendKeys("Password1"); // password
        driver.hideKeyboard(); // hide keyboard
        driver.findElement(By.xpath("//android.widget.Button[@resource-id='com.fuseaware.fuseaware:id/sign_in']")).click(); // sign in
        Thread.sleep(6000);
        driver.findElement(By.xpath("//android.widget.Button[@text='ALLOW']")).click(); // click on allow
        Thread.sleep(3000);
        tap(71, 150);
        Thread.sleep(3000);
        tap(963, 468);
        Thread.sleep(3000);
    }

    @Test(dependsOnMethods = "Login", priority = 2)
    public void validateHomePage() throws Exception {
        Thread.sleep(6000);
        sAssert.assertTrue(driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Emergency SMS\"]")).isDisplayed()); // emergency sms icon
        sAssert.assertEquals(driver.findElement(By.xpath("//android.widget.TextView[@text='Interventions']")).getText(), "Interventions"); //interventions
        sAssert.assertEquals(driver.findElement(By.xpath("//android.widget.TextView[@text='Training']")).getText(), "Training"); // observations
        scroll(716, 780, 690, 217); //scroll down
        sAssert.assertEquals(driver.findElement(By.xpath("//android.widget.TextView[@text='Observations']")).getText(), "Observations"); //observations
        sAssert.assertAll();
    }

    @Test(dependsOnMethods = "Login", priority = 3)
    public void validateAlertSideMenus() throws InterruptedException {
        tap(71, 150);
        //driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout")).click();
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Alerts']")).getText(), "Alerts"); // alerts side menu
        driver.findElement(By.xpath("//*[@text='Alerts']")).click(); // click on alerts side menu
        Thread.sleep(2000); // wait
        sAssert.assertTrue(driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Global message\"]")).isDisplayed()); //global messages icon
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.ImageView[@content-desc='Previous day']"))); //wait until filter symbol appears
        driver.findElement(By.xpath("//android.widget.ImageView[@content-desc='Previous day']")).click(); // click on filter symbol
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='All alerts']")).getText(), "All alerts"); // all alerts from the filter
        driver.findElement(By.xpath("//*[@text='All alerts']")).click(); // click on all alerts
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.ImageView[@content-desc='Previous day']"))); //wait until filter symbol appears
        driver.findElement(By.xpath("//android.widget.ImageView[@content-desc='Previous day']")).click(); // click on filter symbol
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Observations']")).getText(), "Observations"); //all observations from the filter
        driver.findElement(By.xpath("//*[@text='Observations']")).click(); // click on all observations
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.ImageView[@content-desc='Previous day']"))); //wait until filter symbol appears
        driver.findElement(By.xpath("//android.widget.ImageView[@content-desc='Previous day']")).click(); //click on filter symbol
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Interventions']")).getText(), "Interventions"); //all interventions from the filter
        driver.findElement(By.xpath("//*[@text='Interventions']")).click(); // click on all interventions
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.ImageView[@content-desc='Previous day']"))); //wait until filter symbol appears
        driver.findElement(By.xpath("//android.widget.ImageView[@content-desc='Previous day']")).click(); //click on filter symbol
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Global messages']")).getText(), "Global messages"); //global messages from the filter
        driver.findElement(By.xpath("//*[@text='Global messages']")).click(); // click on global messages
        Thread.sleep(2000); // wait
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fuseAware']")), "fuseAware"); // fuseaware header text on home screen
    }

    @Test(dependsOnMethods = "Login", priority = 4)
    public void validateSideMenu() {
        tap(71, 150);
        //My team
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='My team']")).getText(), "My team");
        //fA Connect
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fA Connect']")).getText(), "fA Connect");
        //Training
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Training']")).getText(), "Training");
        //Wellness&Health
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Wellness & Health']")).getText(), "Wellness & Health");
        //Activity Reporting
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Activity Reporting']")).getText(), "Activity Reporting");
        //Evacuation
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Evacuation']")).getText(), "Evacuation");
        //CSCS ID Search
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='CSCS ID Search']")).getText(), "CSCS ID Search");
        scroll(652, 1335, 800, 850);
        //Settings
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Settings']")).getText(), "Settings");
        //Help
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Help']")).getText(), "Help");
        //Log out
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Log out']")).getText(), "Log out");
        scroll(800, 850, 652, 1335);
    }

    @Test(dependsOnMethods = "Login", priority = 5)
    public void validate_sidemenu_faconnect() throws InterruptedException {
        tap(71, 150);
        driver.findElement(By.xpath("//*[@text='fA Connect']")).click();
        Thread.sleep(6000);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fA Connect']")).getText(), "");//fA connect header
        driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Search\"]")).click(); // click on search icon
        driver.findElement(By.xpath("//*[@text='Search']")).click(); // click in search to type
        Thread.sleep(1000); // wait
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Collapse\"]")).click(); // click on back
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ImageButton")).click(); //new message icon
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='New Message']")).getText(), "New Message");//new message header
        driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Search\"]")).click(); //search icon
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.LinearLayoutCompat[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText")).click(); // click in search to type
        Thread.sleep(1000);
        driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Create group\"]")).click();//click on group icon
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='New Group']")).getText(), "New Group");//new group header
        driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"OK\"]")).isDisplayed(); // tick symbol
        Thread.sleep(2000); // wait
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        Thread.sleep(2000); // wait
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Collapse\"]")).click(); // click on back
        driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Create group\"]")).click();//click on group icon
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='New Group']")).getText(), "");//new group header
        driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"OK\"]")).isDisplayed(); // tick symbol
        Thread.sleep(2000); // wait
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        Thread.sleep(2000); // wait
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        Thread.sleep(2000); // wait
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fuseAware']")), "fuseAware"); // fuseaware header text on home screen
    }

    @Test(dependsOnMethods = "Login", priority = 6)
    public void validate_sidemenu_training() throws InterruptedException {
        driver.findElement(By.xpath("//*[@text='Training']")).click();
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Training']")).getText(), "Training"); // training header
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Bookable training']")).getText(), "Bookable training");// bookable training
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Results']")).getText(), "Results");//results
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Generate training code']")).getText(), "Generate training code");// generate training code
        Thread.sleep(2000); // wait
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fuseAware']")), "fuseAware"); // fuseaware header text on home screen
    }

    @Test(dependsOnMethods = "Login", priority = 7)
    public void validate_sidemenu_wellnessandhealth() throws InterruptedException {
        tap(71, 150);
        driver.findElement(By.xpath("//*[@text='Wellness & Health']")).click(); // click on wellness&health side menu
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Wellness & Health']")).getText(), "Wellness&Health");// wellness&health header
        driver.findElement(By.xpath("//*[@text='My Health Data']")).click();//click on my health data
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='My Health Data']")).getText(), "My Health Data");// my health data page header
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        driver.findElement(By.xpath("//*[@text='Construction Health Risks']")).click();//click on construction health risks
        Thread.sleep(2000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Close tab\"]")).click();
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Nutrition Plans']")).getText(), "Nutrition Plans");//nutrition plans
        driver.findElement(By.xpath("//*[@text='Mental Wellbeing']")).click();//click on mental wellbeing
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Mental Wellbeing']")).getText(), "Mental Wellbeing");//mental wellbeing header page
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='REQUEST MEETING']")).getText(), "REQUEST MEETING");//REQUEST MEETING
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        Thread.sleep(2000); // wait
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fuseAware']")), "fuseAware"); // fuseaware header text on home screen
    }

    @Test(dependsOnMethods = "Login", priority = 8)
    public void validate_sidemenu_ativityreporting() throws InterruptedException {
        tap(71, 150);
        driver.findElement(By.xpath("//*[@text='Activity Reporting']")).click();
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Activity Reporting']")).getText(), "Activity Reporting");//activity reporting header
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Report anonymously']")).getText(), "Report anonymously");//report anonymously
        driver.findElement(By.xpath("//*[@text='OFF']")).click(); // click on report anonymously
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@text='ON']")).click(); // click on report anonymously
        Thread.sleep((6000));
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Type *']")).getText(), "Type *");//Type
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Location *']")).getText(), "Location *");//location
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Priority *']")).getText(), "Priority *");//priority
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout/android.widget.FrameLayout/android.widget.EditText")).click();//description
        driver.hideKeyboard(); // hide keyboard
        Thread.sleep(2000);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Take a photo']")).getText(), "Take a photo");//take a photo
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Add from library']")).getText(), "Add from library");//add from library
        Thread.sleep(2000); // wait
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fuseAware']")), "fuseAware"); // fuseaware header text on home screen
    }

    @Test(dependsOnMethods = "Login", priority = 9)
    public void validate_sidemenu_evacuation() throws InterruptedException {
        tap(71, 150);
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@text='Evacuation']")).click(); //click on evacuation side menu
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Evacuation']")).getText(), "Evacuation"); // evacuation header
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Start evacuation']")).getText(), "Start evacuation");//start evacuation
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='START']")).getText(), "START"); //start
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='User report']")).getText(), "User report"); //user report
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fuseAware']")), "fuseAware"); // fuseaware header text on home screen
    }

    @Test(dependsOnMethods = "Login", priority = 10)
    public void validate_sidemenu_cscsidsearch() throws InterruptedException {
        tap(71, 150);
        driver.findElement(By.xpath("//*[@text='CSCS ID Search']")).click();//
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='CSCS ID Search']")).getText(), "CSCS ID Search");//cscs id search header
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/TextInputLayout[1]/android.widget.FrameLayout/android.widget.EditText")).click();//click on cscs id
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/TextInputLayout[2]/android.widget.FrameLayout/android.widget.EditText")).click();//click on last name
        driver.findElement(By.xpath("//*[@text='Or']")).isDisplayed(); // or
        driver.hideKeyboard(); // hide keyboard
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@text='Date of Birth']")).isDisplayed(); // dob
        driver.findElement(By.xpath("//*[@text='SEARCH']")).isDisplayed(); // search
        Thread.sleep(2000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fuseAware']")), "fuseAware"); // fuseaware header text on home screen
    }

    @Test(dependsOnMethods = "Login", priority = 11)
    public void validate_sidemenu_settings() throws InterruptedException {
        tap(71, 150);
        scroll(699, 1224, 618, 652);
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@text='Settings']")).click();//click on settings side menu
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Settings']")).getText(), "Settings");//settings header
        driver.findElement(By.xpath("//*[@text='Edit Profile']")).click(); // click on edit profile
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Edit Profile']")).getText(), "Edit Profile");//edit profile header
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageView")).isDisplayed();//picture is available
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageButton")).isDisplayed();//edit should be displayed next to picture
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout/android.widget.FrameLayout/android.widget.EditText")).click(); // click on first name
        driver.hideKeyboard(); // hide keyboard
        Thread.sleep(2000);
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[2]/android.widget.FrameLayout/android.widget.EditText")).click(); // click on last name
        driver.hideKeyboard(); // hide keyboard
        Thread.sleep(2000);
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[3]/android.widget.FrameLayout/android.widget.EditText")).click(); // click on email id
        driver.hideKeyboard(); // hide keyboard
        Thread.sleep(2000);
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[4]/android.widget.FrameLayout/android.widget.EditText")).click(); // click on phone no
        driver.hideKeyboard(); // hide keyboard
        Thread.sleep(2000);
        scroll(980, 1297, 951, 1382);
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@text='CHANGE PASSWORD']")).click();//click on change password button
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Change password']")).getText(), "Change password");//change password header
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[1]/android.widget.FrameLayout/android.widget.EditText")).click(); // click on current password
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[2]/android.widget.FrameLayout/android.widget.EditText")).click(); // click on new password
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[3]/android.widget.FrameLayout/android.widget.EditText")).click(); // click on confirm password
        Thread.sleep(2000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on x to go back from change password page
        sAssert.assertEquals(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView")).getText(), "Edit Profile");//edit profile header
        Thread.sleep(2000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();//click on x to go back from edit profile page
        sAssert.assertEquals(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView")).getText(), "Settings");//settings header
        Thread.sleep(2000);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Site']")).getText(), "Site");//site header
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Site']")).getText(), "Site");//site sub header
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]")).click();//click on site
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Change site']")).getText(), "Change site");//change site header
        Thread.sleep(2000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();//click on back
        sAssert.assertEquals(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView")).getText(), "Settings");//settings header
        driver.findElement(By.xpath("//*[@text='Add to site']")).click();//click on add to site
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Search user']")).getText(), "Search user");//search user header
        driver.findElement(By.xpath("//*[@text='ID type:']")).click(); // click on id type header
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@text='CSCS']")).click(); // click on cscs from dropdown
        Thread.sleep(1000);
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.Spinner")).click();//click on dropdown
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@text='ECS']")).click();//click on ecs
        Thread.sleep(1000);
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.Spinner")).click();//click on dropdown
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@text='ID type:']")).click();//click on id type
        Thread.sleep(2000);
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/TextInputLayout/android.widget.FrameLayout/android.widget.EditText")).click();//click on edit text to enter id
        Thread.sleep(2000);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='SEARCH']")).getText(), "Search");//search
        Thread.sleep(2000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // back
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Connected accounts']")).getText(), "Connected accounts");//connected accounts
        driver.findElement(By.xpath("//*[@text='Google Fit']")).click();//click on google fit
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Google Fit']")).getText(), "Google Fit");//google fit header
        Thread.sleep(2000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // back
        driver.findElement(By.xpath("//*[@text='Health kiosk']")).click();//click on health kiosk
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Connect to health kiosk']")).getText(), "Connect to health kiosk");//connect to health kiosk header
        Thread.sleep(2000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // back
        driver.findElement(By.xpath("//*[@text='Log out']")).click();//log out
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Log out?']")).getText(), "Log out?"); // log out on pop up
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='LOG OUT']")).getText(), "LOG OUT"); // log out
        driver.findElement(By.xpath("//*[@text='CANCEL']")).click();
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fuseAware']")), "fuseAware"); // fuseaware header text on home screen
    }

    @Test(dependsOnMethods = "Login", priority = 12)
    public void validate_sidemenu_help() throws InterruptedException {
        tap(71, 150);
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        Thread.sleep(3000);
        scroll(652, 1335, 800, 850);
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@text='Help']")).click();//click on help side menu
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Help']")).getText(), "Help");//help header
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Contact Us']")).getText(), "Contact Us");//contact us
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Email']")).getText(), "Email");//email
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='About']")).getText(), "About");//about
        driver.findElement(By.xpath("//*[@text='FAQs']")).click();//FAQs
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='FAQs']")).getText(), "FAQs");//FAQs header
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Using fuseAware']")).getText(), "Using fuseAware");//using fuseaware
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Alerts']")).getText(), "Alerts");//alerts
        scroll(375, 1398, 476, 337);
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        Thread.sleep(5000);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Emergency Messages and Global Messages']")).getText(), "Emergency Messages and Global Messages");//emergency msgs and global msgs
        scroll(375, 1398, 476, 337);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Wellness & Health']")).getText(), "Wellness & Health");//wellness and health
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Support']")).getText(), "Support");//support
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Manage Team']")).getText(), "Manage Team");//manage team
        Thread.sleep(2000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();
        driver.findElement(By.xpath("//*[@text='Open-source licences']")).click();//open source licenses
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Open-source licences']")).getText(), "Open-source licences");//open source licenses header
        Thread.sleep(1000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();
        driver.findElement(By.xpath("//*[@text='App version']")).click(); // click on app version
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Legal']")).getText(), "Legal");//legal
        driver.findElement(By.xpath("//*[@text='Privacy Policy']")).click();//click on privacy policy
        Thread.sleep(1000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Close tab\"]")).click();//click on close
        driver.findElement(By.xpath("//*[@text='Terms of Use Policy']")).click();//click on terms of use policy
        Thread.sleep(1000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Close tab\"]")).click();//click on close
        Thread.sleep(1000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // back from help
        sAssert.assertEquals(element, "fuseAware"); // fuseaware header text on home screen
    }

    @Test(dependsOnMethods = "Login", priority = 13)
    public void validate_sidemenu_logOut() throws InterruptedException {
        tap(71, 150);
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        Thread.sleep(3000);
        scroll(652, 1335, 800, 850);
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@text='Log out']")).click();//click on log out side menu
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Log out?']")).getText(), "Log out?"); // log out on pop up
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='LOG OUT']")).getText(), "LOG OUT"); // log out
        driver.findElement(By.xpath("//*[@text='CANCEL']")).click();
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fuseAware']")), "fuseAware"); // fuseaware header text on home screen
    }


    @Test(dependsOnMethods = "Login", priority = 14)
    public void validateGlobalMessageFromInterventions() {
        // tap(71, 150);
        //  tap(791, 671);
        driver.findElement(By.xpath("//*[@text='Interventions']")).click(); // click on interventions on home screen
        driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS); // wait
        driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Global message\"]")).click(); // click on global messages icon on top
        sAssert.assertEquals(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView")).getText(), "Global message"); // global message header on pop up
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/TextInputLayout/android.widget.FrameLayout/android.widget.EditText")).sendKeys("Test Message"); // enter text message
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]")).click(); // click on send
        driver.manage().timeouts().implicitlyWait(7000, TimeUnit.SECONDS); // wait
        driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Previous day\"]")).click(); // click on filter
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView")).click(); // click on all alerts
        sAssert.assertEquals(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView[1]")).getText(), "Global message"); // global message in alerts
        sAssert.assertEquals(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView[2]")).getText(), "Test Message"); // test msg in alerts
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // back
    }

    @Test(dependsOnMethods = "Login", priority =15)
    public void validateGlobalMessageFromObservations() {
        // tap(71, 150);
        //  tap(791, 671);
        scroll(911, 1653, 787, 562);
        driver.findElement(By.xpath("//*[@text='Observations']")).click();
        driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Global message\"]")).click();
        sAssert.assertEquals(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView")).getText(), "Global message");
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/TextInputLayout/android.widget.FrameLayout/android.widget.EditText")).sendKeys("Test Message");
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]")).click();
        driver.manage().timeouts().implicitlyWait(7000, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Previous day\"]")).click();
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView")).click();
        sAssert.assertEquals(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView[1]")).getText(), "Global message");
        sAssert.assertEquals(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView[2]")).getText(), "Test Message");
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();
    }

    @DataProvider
    public static Object[][] userdetails() {
        return new Object[][] { { "06268591","Hilton Jones"}, { "1234", "Sravani"}, { "21.21_2","1.23A"}};
    }
    @Test(dependsOnMethods = "Login", dataProvider = "userdetails", priority = 16)
    public void validateCSCSIDPage(String cscsid, String Lastname){
         tap(71, 150);
        //  tap(791, 671);
        driver.findElement(By.xpath("//*[@text='CSCS ID Search']")).click();//click on cscs id search in side menu
        driver.manage().timeouts().implicitlyWait(3000,TimeUnit.SECONDS); // wait
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='CSCS ID Search']")).getText(),"CSCS ID Search"); // cscs id search page header
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='CSCS ID']")).getText(),"CSCS ID "); // cscs id
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/TextInputLayout[1]/android.widget.FrameLayout/android.widget.EditText")).sendKeys(cscsid); // enter cscsid
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/TextInputLayout[2]/android.widget.FrameLayout/android.widget.EditText")).sendKeys(Lastname); // enter last name
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Last name']")).getText(),"Last name");
        driver.hideKeyboard();
        driver.findElement(By.xpath("hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Button")).click(); // click on search
        try{
            sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='CSCS #6268591']")).getText(),"CSCS #6268591"); //cscs page header when enetered valid details
        }catch(Exception e){
            sAssert.assertEquals(driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView")).getText(),"No Users");// no users msg should be displayed when entered invalid details
        }
    }

    @Test(dependsOnMethods = "Login", priority = 17)
    public void validate_evacuation_scenario() throws InterruptedException {
        tap(71, 150);
        driver.findElement(By.xpath("//*[@text='Evacuation']")).click(); //click on evacuation side menu
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Evacuation']")).getText(), "Evacuation"); // evacuation header
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Start evacuation']")).getText(), "Start evacuation");//start evacuation
        driver.findElement(By.xpath("//*[@text='START']")).click(); // click on start
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Start evacuation?']")).getText(), "Start evacuation?"); //start evacuation?
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='CANCEL']")).getText(), "CANCEL"); // cancel
        driver.findElement(By.xpath("//*[@text='START']")).click(); // click on start
        Thread.sleep(2000);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Evacuation in progress']")).getText(), "Evacuation in progress"); //evacuation in progress message
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Please leave the site as fast as you can by the nearest safe route']")).getText(), "Please leave the site as fast as you can by the nearest safe route"); //msg on pop up
        driver.findElement(By.xpath("//*[@text='DISMISS']")).click(); // click on dissmiss
        Thread.sleep(2000);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='User report']")).getText(),"User report");//user report header in evacuation page
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='On site']")).getText(),"On site");//onsite users
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Off site']")).getText(),"Off site");//offsite users
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Offline']")).getText(),"Offline");//offline users
        driver.findElement(By.xpath("//*[@text='SHOW MAP']")).click();//click on show map
        Thread.sleep(2000);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='User report']")).getText(),"User report");// user report header
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='ON SITE']")).getText(),"ON SITE");// on site
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='OFF SITE']")).getText(),"OFF SITE");//off site
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='OFFLINE']")).getText(),"OFFLINE");//offline
        driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Refresh\"]")).click();// click on refresh
        Thread.sleep(2000);
driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on back
        driver.findElement(By.xpath("//*[@text='STOP']")).click(); // click on stop
        Thread.sleep(2000);
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Evacuation ended']")).getText(),"Evacuation ended");//
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='It is now safe to return to site']")).getText(),"It is now safe to return to site");//
        driver.findElement(By.xpath("//*[@text='DISMISS']")).click(); // dissmiss
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='Evacuation']")).getText(),"Evacuation");//
        Thread.sleep(2000);
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();
        sAssert.assertEquals(driver.findElement(By.xpath("//*[@text='fuseAware']")), "fuseAware"); // fuseaware header text on home screen
    }

    @DataProvider
    public static Object[][] profiledetails() {
        return new Object[][] { { "Ren","Hilton","",""}, { "Ren", "Hilton", "philip@fuseaware.com","07448643257"}, { "Ren","Hilton", "renmanager@fuseaware.com", "7424920"}};
    }
    @Test(dependsOnMethods = "Login", dataProvider = "profiledetails", priority = 18)
    public void validate_editProfile_scenario(String FName, String LName, String Email, String PhoneNumber) throws InterruptedException {
        tap(71, 150);
        scroll(652, 1335, 800, 850);
        driver.findElement(By.xpath("//*[@text='Settings']")).click();//click on settings side menu
        driver.findElement(By.xpath("//*[@text='Edit Profile']")).click(); // click on edit profile
        if(!(FName.equals(""))) {
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout/android.widget.FrameLayout/android.widget.EditText")).clear(); // clear existing 1st name
            hideKeyboard();
            Assert.assertEquals("May not be empty",driver.findElement(By.xpath("//*[@text='May not be empty']")).getText());
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout/android.widget.FrameLayout/android.widget.EditText")).sendKeys(FName); // enter new name
            hideKeyboard();
        }
        if(!(LName.equals(""))) {
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[2]/android.widget.FrameLayout/android.widget.EditText")).clear();//clear last name
            hideKeyboard();
            Assert.assertEquals("May not be empty",driver.findElement(By.xpath("//*[@text='May not be empty']")).getText());
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[2]/android.widget.FrameLayout/android.widget.EditText")).sendKeys(LName); // enter new name
            hideKeyboard();
        }
        if(!(Email.equals(""))) {
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[3]/android.widget.FrameLayout/android.widget.EditText")).clear();// clear email id
            hideKeyboard();
            Assert.assertEquals("May not be empty",driver.findElement(By.xpath("//*[@text='May not be empty']")).getText());
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[3]/android.widget.FrameLayout/android.widget.EditText")).sendKeys(Email);// enter new email id
            hideKeyboard();
        }
        if(!(PhoneNumber.equals(""))) {
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[4]/android.widget.FrameLayout/android.widget.EditText")).clear();// clear phone number
            hideKeyboard();
            Assert.assertEquals("May not be empty",driver.findElement(By.xpath("//*[@text='May not be empty']")).getText());
            driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/TextInputLayout[4]/android.widget.FrameLayout/android.widget.EditText")).sendKeys(PhoneNumber);// enter new phone number
            hideKeyboard();
        }
        //Add code for save button

    }

    @Test(dependsOnMethods = "Login", priority = 19)
    public void validate_addToSite() throws InterruptedException {
        tap(71, 150);
        scroll(652, 1335, 800, 850);
        driver.findElement(By.xpath("//*[@text='Settings']")).click();//click on settings side menu
        driver.findElement(By.xpath("//*[@text='Add to site']")).click();//click on add to site
        driver.findElement(By.xpath("//*[@text='ID type:']")).click();//click on ID Type
        driver.findElement(By.xpath("//*[@text='ECS']")).click();//click on ecs
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/TextInputLayout/android.widget.FrameLayout/android.widget.EditText")).sendKeys("123");// enter ecs id
        driver.findElement(By.xpath("//*[@text='SEARCH']")).click(); // click on search
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@text='Site']")).click();
        driver.findElement(By.xpath("//*[@text='First name']")).click();
        driver.findElement(By.xpath("//*[@text='Last name']")).click();
        driver.findElement(By.xpath("//*[@text='ECS']")).click();
        scroll(581, 1605, 592, 311);
        driver.findElement(By.xpath("//*[@text='Roles *']")).click();
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[7]/android.widget.CheckBox")).click(); // click on black hat
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.CheckBox")).click();//click on incident report
        driver.findElement(By.xpath("//*[@text='SAVE']")).click(); // click on save
       // driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click(); // click on close
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@text='Contractor *']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.CheckBox")).click(); // click on building company
        driver.findElement(By.xpath("//*[@text='Skill Level *']")).click();
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.CheckBox")).click();
        driver.findElement(By.xpath("//*[@text='Day Rate *']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.CheckBox")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@text='Job Type *']")).click();
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.CheckBox")).click();//click on carpenter
        driver.findElement(By.xpath("//*[@text='SAVE']")).click();
        driver.findElement(By.xpath("//*[@text='ADD']")).click();


    }

    public void hideKeyboard(){
        try{
            driver.hideKeyboard();}
            catch(Exception e){

        }
    }

    public void tap(int x, int y) {
        TouchAction ta = new TouchAction(driver);
        ta.tap(new PointOption().withCoordinates(x, y)).perform();
    }

    public void scroll(int startx, int starty, int endx, int endy) {

        (new TouchAction(driver))
                .press(PointOption.point(startx, starty))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
                .moveTo(PointOption.point(endx, endy))
                .release()
                .perform();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

}
