package com.fuseaware.mobile.tests;

import com.fuseaware.mobile.pages.CarsPage;
import com.fuseaware.mobile.pages.HomePage;
import com.fuseaware.mobile.pages.HotelsPage;
import com.fuseaware.mobile.pages.LoginPage;
import com.fuseaware.mobile.utils.BaseTest;
import com.fuseaware.mobile.utils.DataUtility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

//@Listeners(com.selenium.fuseaware.mobile.utils.Listeners.class)
public class LoginTests extends BaseTest {
	BaseTest test;
	LoginPage loginPage;
	HomePage homePage;
	private static final Logger log = LogManager.getLogger(LoginTests.class);
	@DataProvider(name = "Locations")

	public static Object[][] Locations() {
		return new Object[][] { { "Bangalore"}, { "Chennai"}, { "New Delhi"}};
	}
	SoftAssert assertion = new SoftAssert();
	
	@Test
	public void validateLoginPage() throws InterruptedException {
		loginPage.enterUsername("ren@fuseaware.com");
		loginPage.enterPassword("Password1");
		loginPage.clickOnSignIn();
		loginPage.allowPermisions();
		loginPage.validateHomePage();
	//	homePage = loginPage.clickOnSignIn();
	//	homePage.allowPermisions();
	//	homePage.clickOnOptionsLink();


		//Assert.assertEquals("","");
	}
	@Test(dependsOnMethods = "validateLoginPage")
	public void validateHomePage(){

		homePage.validateHomePage();
	}

	@Test(enabled = false)
	public void verify_data() throws Exception{
		DataUtility.setExcelFile();
		//System.out.println("===================="+DataUtility.setExcelFile());
		
		log.info(" Test case Started");
		log.info(" Navigated to PHP Travels.net");
		//homePage.enterLocation(DataUtility.getCellData(2, 1));
		log.info(" Entered Location");
	//	hotelsPage = homePage.clickOnSearch();
		//Assert.assertEquals(homePage.getResults(),DataUtility.getCellData(2, 2) );
	//	homePage = hotelsPage.clickOnLogo();
		//WebDriverHelper.getscreenshot();
		DataUtility.setCellData("Pass", 2, 3);
		System.out.println("executed");
		
	}
	
	@Test(enabled = false)
	@Parameters({ "hotelLocation" , "results"})
	public void verify_parameter(String Location, String ExpectedResult) throws Exception {
		log.info(" Test case Started");
		log.info(" Navigated to PHP Travels.net");
		//homePage.enterLocation(Location);
		log.info(" Entered Location");
	//	hotelsPage = homePage.clickOnSearch();
		//Assert.assertEquals(homePage.getResults(), ExpectedResult);
	//	homePage = hotelsPage.clickOnLogo();
		//WebDriverHelper.getscreenshot();
		System.out.println("executed");
	/*	SoftAssert assertion = new SoftAssert();
		assertion.assertEquals(true, false);
		assertion.assertEquals(false, false);
		assertion.assertEquals(true, true);
		assertion.assertAll();
		
		assertion.assertEquals(true, true);
		System.out.println("Assertion 1");
		assertion.assertEquals(true, false);
		System.out.println("Assertion 2");
		assertion.assertEquals(true, true);
		System.out.println("Assertion 3");
		assertion.assertAll();*/
		
	}

	@Test(dataProvider = "Locations", enabled=false)
	public void verify(String Location) throws InterruptedException {
		System.out.println("executed");
		System.out.println(" data provider "+ Location);
		/*homePage.enterLocation(Location);
		hotelsPage = homePage.clickOnSearch();
		Assert.assertEquals(homePage.getResults(), "No Results!!");
		homePage = hotelsPage.clickOnLogo();
		assertion.assertEquals("", "");*/
	}

	@BeforeClass
	public void setup() {
		loginPage = new LoginPage(getDriver());
	}

}
