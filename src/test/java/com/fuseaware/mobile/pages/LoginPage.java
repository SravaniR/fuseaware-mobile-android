package com.fuseaware.mobile.pages;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class LoginPage extends BasePage {
    //  -- temp
    @FindBy(xpath="//android.widget.Button[@text='ALLOW']")
    private WebElement _permissionAllow;

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc='Open navigation drawer']")
    private WebElement _optionsLink;

    @FindBy(xpath = "//android.widget.TextView[@text='fuseAware']")
    private WebElement _pageTitle;

    @FindBy(xpath = "//android.widget.TextView[@text='Interventions']")
    private WebElement _txtInterventions;

    @FindBy(xpath = "//android.widget.TextView[@text='Training']")
    private WebElement _txtTraining;

    @FindBy(xpath = "//android.widget.TextView[@text='Observations']")
    private WebElement _txtObservations;

    // -- temp

    @FindBy(xpath="//android.widget.EditText[@resource-id='com.fuseaware.fuseaware:id/email_or_phone']")
    private WebElement _inputBoxUsername;

    @FindBy(xpath="//android.widget.EditText[@resource-id='com.fuseaware.fuseaware:id/password']")
    private WebElement _inputBoxPassword;

    @FindBy(xpath="//android.widget.Button[@resource-id='com.fuseaware.fuseaware:id/sign_in']")
    private WebElement _btnSignIn;

    public LoginPage(AndroidDriver<AndroidElement> driver) {
        super(driver);
    }

    public void enterUsername(String username){
        _inputBoxUsername.sendKeys(username);
    }

    public void enterPassword(String password){
        _inputBoxPassword.sendKeys(password);
        driver.hideKeyboard();
    }

    public HomePage clickOnSignIn() {
        _btnSignIn.click();
        return new HomePage(getDriver());
    }



// temp

    public void validateHomePage() throws InterruptedException {

        //softAssert = new SoftAssert();
       /* TouchAction ta= new TouchAction(getDriver());
        ta.tap(new PointOption().withCoordinates(480, 386)).perform();
        Thread.sleep(3000);*/
        System.out.println(driver.getContext());
        Assert.assertEquals(_txtInterventions.getText(), "Interventions");
        Assert.assertEquals(_txtTraining.getText(), "Training");
        Assert.assertEquals(_txtObservations.getText(),"Observations");

    }

    public void allowPermisions() {
        try {
            Thread.sleep(8000);
            _permissionAllow.click();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}