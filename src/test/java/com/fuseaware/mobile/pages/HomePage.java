package com.fuseaware.mobile.pages;

import java.util.concurrent.TimeUnit;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class HomePage extends BasePage {

	AndroidDriver<AndroidElement> driver;
	SoftAssert softAssert;

	@FindBy(xpath="//android.widget.Button[@text='ALLOW']")
	private WebElement _permissionAllow;

	@FindBy(xpath = "//android.widget.ImageButton[@content-desc='Open navigation drawer']")
	private WebElement _optionsLink;

	@FindBy(xpath = "//android.widget.TextView[@text='fuseAware']")
	private WebElement _pageTitle;

	@FindBy(xpath = "//android.widget.TextView[@text='Interventions']")
	private WebElement _txtInterventions;

	@FindBy(xpath = "//android.widget.TextView[@text='Training']")
	private WebElement _txtTraining;

	@FindBy(xpath = "//android.widget.TextView[@text='Observations']")
	private WebElement _txtObservations;

	public HomePage(AndroidDriver<AndroidElement> driver) {
		super(driver);
		this.driver=driver;
	}

	public OptionsPage clickOnOptionsLink(){
		_optionsLink.click();
		return new OptionsPage(getDriver());
	}

	public void validateHomePage() {

		//softAssert = new SoftAssert();
		Assert.assertEquals(_txtInterventions.getText(),"Interventions");
		Assert.assertEquals(_txtTraining.getText(),"Training");
		Assert.assertEquals(_txtObservations.getText(),"Observations");

	}

	public void allowPermisions() {
		try {
			Thread.sleep(8000);
			_permissionAllow.click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		/*while (_permissionAllow.isDisplayed()) {
			_permissionAllow.click();
		}*/
	}



	public void implicitWaits(){
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

}
