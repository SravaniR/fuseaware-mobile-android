package com.fuseaware.mobile.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class CarsPage extends BasePage{
	
	public CarsPage(AndroidDriver<AndroidElement> driver){
		super(driver);
	}

	public void selectItem() {
		System.out.println("Navigated to Cars Page");
		
	}

}
