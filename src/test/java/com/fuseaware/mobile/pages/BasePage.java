package com.fuseaware.mobile.pages;

import com.fuseaware.mobile.utils.AndroidDriverHelper;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasePage{
	AndroidDriver<AndroidElement> driver;
	
	public BasePage(AndroidDriver<AndroidElement> driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);

	}
	
	public void setDriver(AndroidDriver<AndroidElement> driver){
		this.driver=driver;
	}
	
	public AndroidDriver<AndroidElement> getDriver(){
		return this.driver;
	}
	
}
