package com.fuseaware.mobile.utils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

public class BaseTest {
	private AndroidDriver<AndroidElement> driver;
	
	@BeforeSuite
	public void beforeAllClasses() throws Exception{
		driver= AndroidDriverHelper.createDriver();
	}

	public AndroidDriver<AndroidElement> getDriver(){
		return driver;
	}
	
	@AfterSuite
	public void tearDown(){
		driver.quit();
	}
	
	

}
