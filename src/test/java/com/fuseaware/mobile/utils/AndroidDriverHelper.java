package com.fuseaware.mobile.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;
import java.net.URL;
import java.util.logging.Level;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;

public class AndroidDriverHelper {
	private static String reportDirectory = "reports";
	private static String reportFormat = "xml";
	private static String testName = "Untitled";
	protected static AndroidDriver<AndroidElement> driver = null;
	protected static Properties _userProperties = new Properties();

	public AndroidDriverHelper() throws IOException{
		loadProperties();
	}

	private static Properties loadProperties() throws IOException {
		try{
			FileInputStream configStream = new FileInputStream("config.user.properties");
			_userProperties.load(configStream);
			return _userProperties;
		} catch (FileNotFoundException e){
			System.out.println("No config file Found");
		}
		return _userProperties;
	}
	
	public static String getStringProperty(String propertyname) throws FileNotFoundException{
		try{
			_userProperties = loadProperties();
		}catch(IOException e){
			e.printStackTrace();
		}
		String returnValue = _userProperties.getProperty(propertyname);
		return returnValue;
	}

	public static AndroidDriver<AndroidElement> createDriver() throws Exception {
		if (driver == null) {
			/*AndroidDriver<AndroidElement> driver;
			DesiredCapabilities dc = new DesiredCapabilities();
			dc.setCapability("reportDirectory", reportDirectory);
			dc.setCapability("reportFormat", reportFormat);
			dc.setCapability("testName", testName);
			dc.setCapability("deviceName", "Android");
			dc.setCapability(MobileCapabilityType.UDID, "ZY322ZWN37");
			dc.setCapability(MobileCapabilityType.APP, "C:\\Gitlab\\hybridfw-master\\fuseaware_qa.apk");
			dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.fuseaware.fuseaware");
			dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".LauncherActivity");
			//dc.setCapability(MobileCapabilityType.NO_RESET, true);
			driver = new AndroidDriver<AndroidElement>(new URL("http://localhost:4723/wd/hub"), dc);
			driver.setLogLevel(Level.INFO);*/
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability("deviceName", "Android");
			cap.setCapability("platformName", "Android");
			cap.setCapability("platformVersion", "7.0");
			cap.setCapability("noReset", "false");
			//cap.setCapability("appPackage","com.fuseaware.fuseaware");
			//cap.setCapability("appactivity","LauncherActivity");
			//cap.setCapability("app", "C:\\Gitlab\\hybridfw-master\\fuseaware_qa.apk");
			cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.fuseaware.fuseaware");
			cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "LauncherActivity");

			//cap.setCapability("app", "C:\\Gitlab\\hybridfw-master\\fuseaware_qa.apk");
			// driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"),cap);
			driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
			System.out.println(driver.getContext());
		}
		return driver;
	}

	public static void getscreenshot() throws Exception {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("C:\\Users\\Hp\\Appium_Demo\\hybridfw\\screenshots\\failure"+System.currentTimeMillis()+".png"));
	}

	public static void getscreenshot(String MethodName) throws Exception {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("C:\\Users\\Hp\\Appium_Demo\\hybridfw\\screenshots\\screenshot.png"));
		FileUtils.copyFile(scrFile, new File("C:\\Users\\Hp\\Appium_Demo\\hybridfw\\screenshots\\"+MethodName+".png"));
		//FileUtils.copyFile(scrFile, new File("C:\\Users\\Hp\\Appium_Demo\\hybridfw\\screenshots\\"+System.currentTimeMillis()+".png"));
		/* try {
			 	String filePath="C:\\Users\\Hp\\Appium_Demo\\hybridfw\\screenshots\\";
				FileUtils.copyFile(scrFile, new File(filePath+methodName+".png"));
				System.out.println("***Placed screen shot in "+filePath+" ***");
			} catch (IOException e) {
				e.printStackTrace();
			}
 }*/
	}
	
	

}
